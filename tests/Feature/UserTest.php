<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\User;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    public function test_a_user_can_list_all_users()
    {
        // Given we have users in databse
        $user = User::factory()->create();

        // When user list all users
        $response = $this->get('/api/users');

        // User should be able to list all users
        $response->assertSee($user->name);
    }

    public function test_a_user_can_view_a_user()
    {
        // Given we have users in databse
        $user = User::factory()->create();

        // When user list a user
        $response = $this->get('/api/users/1');

        // User should be able to view a user
        $response->assertSee($user->name);
    }

    public function test_a_user_can_update_a_user()
    {
        // Given we have users in databse
        $user = User::factory()->create();

        // When user update user
        $response = $this->put('/api/users/1?name=nuno barao&email=nunoandrebarao@outlook.pt&birthday=1986-02-11&gender=m');

        // User should be able to update user
        $response->assertSee($user->name);
    }

    public function test_a_user_can_delete_a_user()
    {
        // Given we have users in databse
        $user = User::factory()->create();

        // When user destroy user
        $response = $this->delete('/api/users/1');

        // User should be able to see destroyed user
        $response->assertSee($user->name);
    }

    public function test_a_user_requires_a_name()
    {
        $this->actingAs(User::factory()->create());

        $user = User::factory()->make(['name' => null]);

        $this->post('/api/users', $user->toArray())
             ->assertSessionHasErrors('name');
    }

    public function test_a_user_requires_an_email()
    {
        $this->actingAs(User::factory()->create());

        $user = User::factory()->make(['email' => null]);

        $this->post('/api/users', $user->toArray())
             ->assertSessionHasErrors('email');
    }

    public function test_a_user_requires_a_birthday()
    {
        $this->actingAs(User::factory()->create());

        $user = User::factory()->make(['birthday' => null]);

        $this->post('/api/users', $user->toArray())
             ->assertSessionHasErrors('birthday');
    }

    public function test_a_user_requires_a_gender()
    {
        $this->actingAs(User::factory()->create());

        $user = User::factory()->make(['gender' => null]);

        $this->post('/api/users', $user->toArray())
             ->assertSessionHasErrors('gender');
    }

    public function test_a_user_requires_a_password()
    {
        $this->actingAs(User::factory()->create());

        $user = User::factory()->make(['password' => null]);

        $this->post('/api/users', $user->toArray())
             ->assertSessionHasErrors('password');
    }

    public function test_authenticated_user_can_create_a_new_user()
    {
        // Given we have authenticated user
        $this->actingAs(User::factory()->create());
        // a user 
        $user = User::factory()->create();
        //When user submits post request to create a new user endpoint
        $this->post('/api/users', $user->toArray());
        // It gets stored in the database, 2 because we have created
        // 1 user for creating another user
        $this->assertEquals(2, User::all()->count());
    }
}
