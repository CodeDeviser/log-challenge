<?php

namespace App\Services;

use Validator;
use App\Services\ResponseService;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserService extends ResponseService {

    /**
     * Display a listing of the users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
    
        return $this->sendResponse($users,  'List of Users.'); 
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'birthday' => 'required',
            'gender' => 'required',
            'password' => 'required|string|min:8',
        ]);
            
        $user = User::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'birthday' => $validatedData['birthday'],
            'gender' => $validatedData['gender'],
            'password' => Hash::make($validatedData['password']),
        ]);
            
        $token = $user->createToken('auth_token')->plainTextToken;
        
        return $this->sendResponse([
                'access_token' => $token,
                'token_type' => 'Bearer',
            ],  'User Registed successfully.');       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        return $this->sendResponse($user, 'Show user successfully.');
    } 
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update($request, $user)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required',
            'birthday' => 'required',
            'gender' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->birthday = $input['birthday'];
        $user->gender = $input['gender'];
        $user->save();
   
        return $this->sendResponse($user, 'User updated successfully.');
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return $this->sendResponse($user, 'User deleted successfully.');  
    }
}