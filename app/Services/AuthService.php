<?php

namespace App\Services;

use App\Services\ResponseService;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthService extends ResponseService
{
    public function login($request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return $this->sendResponse([], 'Invalid Login Details');
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        return $this->sendResponse([
            'access_token' => $token,
            'token_type' => 'Bearer',
         ], 'User Logged in successfully.');
    }
}
