<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\AuthService;

class AuthController extends BaseController
{

    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Login user
     * 
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        return $this->authService->login($request);
    }

}
