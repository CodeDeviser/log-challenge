## Setup project

Clone project to your machine using:

```
git clone https://gitlab.com/CodeDeviser/log-challenge
```

Go to project roots folder and run:
```
composer install
```

Create .env file from .env.example

After that generate a new key for your project:
```
php artisan key:generate
```

Configure your databse on .env file:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=log_local
DB_USERNAME=nuno
DB_PASSWORD=root
```

Create a database and run migrations and seeds:
```
php artisan migrate:fresh --seed
```

Start the server and make sure that is running on port 8000:
```
php artisan serve
```

To test the endpoint's use postman collection that is on project roots folder:
```
log-challenge.postman_collection.json
```

On Postman, pick one user email from database that we have seeded earlier and use those credentials on postamn body form data to login and generate a token.

Note: password will be for any user "12345678".

Grab the token, copy and paste to all endpoint's authentication bearer token.

After this you should be able to access and test endpoint's

To run Tests use:
```
 ./vendor/bin/phpunit
```

To generate reports use:
```
 ./vendor/bin/phpunit --coverage-html reports
```
The reports will output to "reports" on project roots folder

Note: you need to have xdebug extension on your php.ini
